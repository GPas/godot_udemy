extends Node2D
class_name GameState

export var starting_lives : int = 3
export var coin_target : int = 10

var lives : int
var coins : int = 0

onready var GUI = Global.GUI

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.GameState = self
	lives = starting_lives
	update_GUI()

func update_GUI():
	GUI.update_GUI(coins, lives)

func hurt():
	lives -= 1
	Global.Player.hurt()
	update_GUI()
	GUI.animate("Hurt")
	Global.Pain_SFX.play()
	if lives < 0:
		end_game()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func end_game():
	get_tree().change_scene(Global.GameOver)

func win_game():
	get_tree().change_scene(Global.Victory)

func coin_up():
	coins += 1
	GUI.animate("CoinPulse")
	var multiple_of_coin_target = (coins % coin_target) == 0
	if multiple_of_coin_target:
		life_up()
	update_GUI()

func life_up():
	lives += 1
	Global.LifeUp_SFX.play()
	GUI.animate("LifePulse")