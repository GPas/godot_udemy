extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.Jump_SFX = $Jump_SFX
	Global.Coin_SFX = $Coin_SFX
	Global.Pain_SFX = $Pain_SFX
	Global.LifeUp_SFX = $LifeUp

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
