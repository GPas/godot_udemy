extends CanvasLayer
class_name GUI

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.GUI = self

func update_GUI(coins : int, lives : int):
	$Banner/HBoxContainer/CoinCount.text  = str(coins)
	$Banner/HBoxContainer/LifeCount.text = str(lives)

func animate(animation : String):
	$AnimationPlayer.queue(animation)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
