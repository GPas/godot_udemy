extends "res://Scripts/Door.gd"

var combination = []
var first_try = true

func _ready():
	pass

func _input_event(viewport, event, shape_idx):
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and can_click:
		if is_open and get_overlapping_bodies_without(Global.Player).empty():
			close()
		elif first_try:
			$CanvasLayer/NumberPad.popup_centered()
		else:
			open()

func _on_Door_body_exited(body):
	if body == Global.Player:
		can_click = false
		$CanvasLayer/NumberPad.hide()
		$CanvasLayer/NumberPad.reset_lock()
	if get_overlapping_bodies_without(body).empty():
		close()

func _on_NumberPad_combination_correct():
	open()
	first_try = false

func _on_Computer_combination(numbers, lock_group):
	combination = numbers
	$CanvasLayer/NumberPad.combination = combination
	$Label.rect_rotation = -rotation_degrees
	$Label.text = lock_group
