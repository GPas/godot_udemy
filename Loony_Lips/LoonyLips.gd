extends Node2D

var player_words = []
var current_story
var description_strings

func _ready():
	set_random_story()
	description_strings = get_from_json("description_strings.json")
	$Blackboard/StoryText.text = description_strings.intro
	prompt_player()
	$Blackboard/TextBox.text = ""
	$Blackboard/TextureButton/RichTextLabel.text = description_strings.ok
	
func set_random_story():
	var stories = get_from_json("stories.json")
	randomize()
	current_story = stories[randi() % stories.size()]

func _on_TextureButton_pressed():
	if is_story_done():
		get_tree().reload_current_scene()
	else:
		_on_TextBox_text_entered($Blackboard/TextBox.text)

func _on_TextBox_text_entered(new_text):
	player_words.append(new_text)
	$Blackboard/TextBox.text = ""
	$Blackboard/StoryText.text = ""
	check_player_word_length()

func is_story_done():
	return player_words.size() == current_story.prompt.size()

func prompt_player():
	$Blackboard/StoryText.text += (description_strings.prompt % current_story.prompt[player_words.size()])

func check_player_word_length():
	if is_story_done():
		tell_story()
	else:
		prompt_player()

func tell_story():
	$Blackboard/StoryText.text = current_story.story % player_words
	$Blackboard/TextureButton/RichTextLabel.text = description_strings.again
	end_game()

func end_game():
	$Blackboard/TextBox.queue_free()
	
func get_from_json(filename):
	var file = File.new()
	file.open(filename, File.READ) #Todo: Check if file exists
	var text = file.get_as_text()
	file.close()
	return parse_json(text)