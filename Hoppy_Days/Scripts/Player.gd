extends KinematicBody2D
class_name Player

const SPEED = 750
const GRAVITY = 3600
const UP = Vector2(0, -1)
const JUMP_SPEED = -1600
const JUMP_BOOST = 2

var respawnposition : Vector2
var motion = Vector2()
export var world_limit = 3000

func _ready():
	Global.Player = self
	respawnposition = self.transform.get_origin()
	
func _physics_process(delta):
	update_motion(delta)
	
func update_motion(delta):
	fall(delta)
	run()
	jump()
	move_and_slide(motion, UP)
	
func fall(delta):
	if is_on_floor() || is_on_ceiling():
		motion.y = 0
	else:
		motion.y += GRAVITY * delta
		
	if position.y > world_limit:
		self.transform.origin = respawnposition
		Global.GameState.hurt()
	
func run():
	if Input.is_action_pressed("ui_right") && !Input.is_action_pressed("ui_left"):
		motion.x = SPEED
	elif Input.is_action_pressed("ui_left") && !Input.is_action_pressed("ui_right"):
		motion.x = -SPEED
	else:
		motion.x = 0
		
func jump():
	if is_on_floor() && Input.is_action_pressed("ui_up"):
		motion.y = JUMP_SPEED
		Global.Jump_SFX.play()

func boost():
	motion.y = JUMP_SPEED * JUMP_BOOST
	move_and_slide(motion, UP)

func hurt():
	motion.y = JUMP_SPEED
	move_and_slide(motion, UP)

func _process(delta):
	update_animation(motion)
	
func update_animation(motion):
	$AnimatedSprite.updateMotion(motion)

func hide():
	self.visible = false
