extends Node2D

var timeout = false

func _process(delta):
	if $Sprite/RayCast2D.is_colliding():
		fire()

func fire():
	if not timeout:
		var lightning = load(Global.Lightning).instance()
		add_child(lightning)
		lightning.position = $Sprite.position 
		$Sprite/Timer.start()
		timeout = true
		print("Fire")

func _on_Timer_timeout():
	timeout = false
