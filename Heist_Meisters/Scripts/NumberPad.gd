extends Popup

onready var display = $VSplitContainer/DisplayContainer/Display
onready var light = $VSplitContainer/ButtonContainer/ButtonGrid/Light

const orange = Color("fa8132")
const green = Color("88e060")

var combination = []
var guess = []

signal combination_correct

func _ready():
	connect_buttons()
	reset_lock()

func connect_buttons():
	for child in $VSplitContainer/ButtonContainer/ButtonGrid.get_children():
		if child is Button:
			child.connect("pressed", self, "_on_Button_pressed", [child.text])

func _on_Button_pressed(text):
	if text == "Ok":
		check_guess()
	else:
		$ButtonSound.play()
		enter(int(text))

func check_guess():
	if guess == combination:
		light.modulate = green
		$SuccessSound.play()
		yield($SuccessSound, "finished")
		hide()
		emit_signal("combination_correct")
	reset_lock()

func enter(number):
	guess.append(number)
	update_display()

func reset_lock():
	guess.clear()
	display.clear()
	light.modulate = orange

func update_display():
	display.bbcode_text = "[center]" + PoolStringArray(guess).join("") + "[/center]"
	if guess.size() == combination.size():
		check_guess()