extends AnimatedSprite

func _on_Area2D_body_entered(body):
	$Area2D.set_deferred("monitoring", false)
	Global.GameState.coin_up()
	$AnimationPlayer.play("Die")
	Global.Coin_SFX.play()

func  die():
	queue_free()
	
func _ready():
	self.playing = true;
