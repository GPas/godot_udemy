extends Area2D

const SPEED = 200

func _ready():
	pass

func _physics_process(delta):
	move_down_screen(delta)
	manage_collision()
	if not $VisibilityNotifier2D.is_on_screen():
		queue_free()

func move_down_screen(delta : float):
	position.y += SPEED * delta

func manage_collision():
	var collider = get_overlapping_bodies()
	for body in collider:
		if body == Global.Player:
			Global.GameState.hurt()
		queue_free()