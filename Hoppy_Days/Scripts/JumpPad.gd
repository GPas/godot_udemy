extends Area2D

func _ready():
	$AnimatedSprite.play("idle")

func _on_JumpPad_body_entered(body):
	$AnimatedSprite.play("spring")
	$Timer.start()
	Global.Player.boost()
	$AudioStreamPlayer2D.play()

func _on_Timer_timeout():
	$AnimatedSprite.play("idle")
