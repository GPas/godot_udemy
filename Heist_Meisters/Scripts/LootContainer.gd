extends NinePatchRect

func _ready():
	hide()

func collect_loot():
	show()
	$VBoxContainer2/LootList.add_icon_item(load(Global.briefcase_sprite), false)