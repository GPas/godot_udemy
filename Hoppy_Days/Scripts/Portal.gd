extends Area2D

var timeout = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Portal_body_entered(body):
	$AudioStreamPlayer.play()
	Global.Player.hide()
	yield($AudioStreamPlayer, "finished")
	Global.GameState.win_game()
