extends "res://Scripts/Character.gd"

const FOV_TOLERANCE = 22.5
const MAX_DETECTION_RANGE = 320

onready var Player = Global.Player

func _ready():
	add_to_group("npc")

func _process(delta):
	if player_is_in_FOV_TOLERANCE() and player_is_in_LOS():
		$Torch.color = Color.red
		get_tree().call_group("SuspicionMeter", "player_seen")
	else:
		$Torch.color = Color.white

func player_is_in_FOV_TOLERANCE():
	var NPC_facing_direction = Vector2(1,0).rotated(global_rotation)
	var direction_to_player = (Player.position - global_position).normalized()
	
	if abs(direction_to_player.angle_to(NPC_facing_direction)) < deg2rad(FOV_TOLERANCE):
		return true
	else:
		return false

func player_is_in_LOS():
	var space = get_world_2d().direct_space_state
	var LOS_obstacle = space.intersect_ray(global_position, Player.global_position, [self], collision_mask)
	if not LOS_obstacle:
		return false
	var distance_to_player = Player.global_position.distance_to(global_position)
	var player_in_range = distance_to_player < MAX_DETECTION_RANGE
	
	if LOS_obstacle.collider == Player and player_in_range:
		return true
	else:
		return false

func toggle_torch():
	$Torch.enabled = !$Torch.enabled