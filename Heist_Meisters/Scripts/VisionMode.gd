extends CanvasModulate

const DARK = Color("05082f")
const NIGHTVISION = Color("3bdd14")
onready var audiostreamplayer = $AudioStreamPlayer
onready var nightvision_sfx = load(Global.nightvision_on_sfx)
onready var nightvision_off_sfx = load(Global.nightvision_off_sfx)

func _ready():
	audiostreamplayer.volume_db = -5
	add_to_group("interface")
	color = DARK
	get_tree().call_group("labels", "hide")

func NightVision():
	color = NIGHTVISION
	play_sfx(nightvision_sfx)
	toggle_torches()
	get_tree().call_group("labels", "show")

func DarkVision():
	color = DARK
	play_sfx(nightvision_sfx)
	toggle_torches()
	get_tree().call_group("labels", "hide")

func play_sfx(resource):
	$AudioStreamPlayer.stream = resource
	$AudioStreamPlayer.play()

func toggle_torches():
	get_tree().call_group("npc", "toggle_torch")