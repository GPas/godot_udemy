extends CanvasLayer

onready var label = $CenterContainer/NinePatchRect/VBoxContainer/Label

func _ready():
	if Global.nextLevel == "":
		show_victory()
	else:
		show_next_button()

func _on_NextLevel_pressed():
	get_tree().change_scene(Global.nextLevel)

func show_victory():
	$CenterContainer/NinePatchRect/VBoxContainer/GameEndContainer.show()
	label.text = "You finished Heist Meisters!"

func show_next_button():
	$CenterContainer/NinePatchRect/VBoxContainer/NextLevelContainer.show()
	label.text = "Success!"

func _on_GameEnd_pressed():
	get_tree().change_scene("res://Scenes/SplashScreen.tscn")
