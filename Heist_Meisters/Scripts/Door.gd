extends Area2D

var can_click = false
var is_open = false
onready var door_close_sfx = load(Global.door_close_sfx)
onready var door_open_sfx = load(Global.door_open_sfx)

func _input_event(viewport, event, shape_idx):
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and can_click:
		if is_open and get_overlapping_bodies_without(Global.Player).empty():
			close()
		else:
			open()

func _on_Door_body_entered(body):
	if not body == Global.Player and not is_open:
		open()
	else:
		can_click = true

func _on_Door_body_exited(body):
	if body == Global.Player:
		can_click = false
	if get_overlapping_bodies_without(body).empty():
		close()

func open():
	if not is_open:
		$AnimationPlayer.play("open")
		$AudioStreamPlayer2D.stream = door_open_sfx
		$AudioStreamPlayer2D.play()
		is_open = true

func close():
	if is_open:
		$AnimationPlayer.play_backwards("open")
		$AudioStreamPlayer2D.stream = door_close_sfx
		$AudioStreamPlayer2D.play()
		is_open = false
	
func get_overlapping_bodies_without(body):
	var overlapping_bodies = get_overlapping_bodies()
	overlapping_bodies.erase(body)
	return overlapping_bodies