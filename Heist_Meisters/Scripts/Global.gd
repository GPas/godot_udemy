extends Node

var Player
var navigation
var destinations
var nextLevel = ""

const nightvision_on_sfx = "res://SFX/nightvision.wav"
const nightvision_off_sfx = "res://SFX/nightvision_off.wav"
const door_open_sfx = "res://SFX/doorOpen_1.ogg"
const door_close_sfx = "res://SFX/doorClose_4.ogg"

const box_sprite = "res://GFX/PNG/Tiles/tile_129.png"
const box_occluder = "res://Scenes/Characters/BoxOccluder.tres"
const box_collider = "res://Scenes/Characters/BoxCollider.tres"
const player_sprite = "res://GFX/PNG/Hitman 1/hitman1_stand.png"
const player_occluder = "res://Scenes/Characters/CharacterOccluder.tres"
const player_collider = "res://Scenes/Characters/CharacterCollider.tres"

const briefcase_sprite = "res://GFX/Loot/suitcase.png"